

function AfficherBoiteDialogue() {
    let condition = false;

    do {
        let annee = prompt("Entrez une année sous forme YYYY:")
        if ((IsNaN(annee) === true) || (annee.length != 4) || (annee.length <= 0)) {
            condition = false;
        } else {
            condition = true;
            EstBissextile(annee);
        }
    } while (condition === false);
}

function EstBissextile(annee) {
    if ((annee % 4 === 0 && annee % 100 > 0) || (annee % 400 === 0)) {
        document.getElementById("zoneSaisie").innerHTML =
            "<p style='color:green'> L'année" + annee + " : est une année bissextile</p>";
    } else {
        document.getElementById("zoneSaisie").innerHTML =
            "<p style='color:red'> L'année" + annee + " : n'est pas une année bissextile</p>";
    }

}